
FROM alpine:latest
ARG VER
RUN apk add --no-cache jq openssh-client
RUN apk add --no-cache "ansible=$VER"

CMD [""]
